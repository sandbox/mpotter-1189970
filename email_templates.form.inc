<?php
// $Id: email_templates.form.inc,v 1.0 2011/06/16 13:07:00 mpotter Exp $

/**
 * @file
 * Form for selecting email template and sending email.
 */

/**
 * Email selection and sending form.
 */
function email_templates_form() {
  $options = array();
  $result = db_query("SELECT nid, title from {node} WHERE type = 'email_template'");
  while( $node = db_fetch_object( $result)) {
    $options[$node->nid] = check_plain($node->title);
  }

  $form['template'] = array(
    '#type' => 'select',
    '#title' => t('Select email template'),
    '#options' => $options,
    '#default_value' => 0,
  );
  $form['job_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Job Type'),
    '#default_value' => '',
  );
  $form['customer_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Customer Name'),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form['customer_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Customer Email'),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form['appt_date'] = array(
    '#type' => 'date',
    '#title' => t('Consultation Date'),
    '#required' => TRUE,
  );
  $form['appt_time'] = array(
    '#type' => 'time',
    '#title' => t('Consultation Time'),
    '#granularity' => variable_get('email_template_granularity', '15'),
    '#required' => TRUE,
  );
  $form['appt_endtime'] = array(
    '#type' => 'time',
    '#title' => t('Consultation Ending Time'),
    '#granularity' => variable_get('email_template_granularity', '15'),
    '#required' => FALSE,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send Email'),
  );

  return $form;
}

/**
 * Validate email confirmation form.
 */
function email_templates_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['customer_email'])) {
      form_set_error('mail', t('You must enter a valid e-mail address for the customer.'));
  }
  $tid = $form_state['values']['template'];
  $node = node_load($tid);
  if (!$node) {
      form_set_error('email_templates', t('Could not load the selected template.'));
  }
  // save template node for later (in submit) so we don't have to load the node more than once
  $form_state['values']['node'] = $node;
  if (empty($form_state['values']['customer_name'])) {
      form_set_error('email_templates', t('You must enter a customer name.'));    
  }
}

/**
 * Submit email confirmation form
 */
function email_templates_form_submit($form, &$form_state) {
  $customer_name = check_plain( $form_state['values']['customer_name']);
  $customer_email = check_plain( $form_state['values']['customer_email']);
  

  // make sure timezone is set
  date_default_timezone_set(variable_get('date_default_timezone', @date_default_timezone_get()));

  // compute unix timestamp for appointment
  $appt_date = $form_state['values']['appt_date'];
  $appt_time = $form_state['values']['appt_time'];
  $hour = $appt_time['hour'];
  if ($appt_time['meridiem'] == 'pm') { $hour = $hour + 12; }
  $appt_datetime = mktime( $hour, $appt_time['minute'], 0, 
    $appt_date['month'], $appt_date['day'], $appt_date['year']);
    
  // fields to be stored in template session database
  $fields = array(
    'customer_name' => $customer_name,
    'customer_email' => $customer_email,
    'appt_time' => $appt_datetime,
    'tid' => $form_state['values']['template'],
  );
  // data to be serialized into database
  $appt_endtime = $form_state['values']['appt_endtime'];
  $hour = $appt_endtime['hour'];
  if ($appt_endtime['meridiem'] == 'pm') { $hour = $hour + 12; }
  $appt_enddatetime = mktime( $hour, $appt_endtime['minute'], 0, 
    $appt_date['month'], $appt_date['day'], $appt_date['year']);
  $data = array(
    'job_type' => check_plain( $form_state['values']['job_type']),
    'appt_endtime' => $appt_enddatetime,
  );
  $template_node = email_templates_store_session( $fields, $data);
  // other fields used for tokens but not stored in database
  $template_node->job_type = check_plain( $form_state['values']['job_type']);

  // build email from template
  $template = $form_state['values']['node'];
  $body = $template->body;
  $subject = $template->field_subject[0]['value'];
  
  $header = variable_get('email_template_header', '');
  $footer = variable_get('email_template_footer', '');
  // perform our own Header and Footer replacement so token_replace will expand any tokens within the header and footer
  $body = str_replace( '[email_header]', $header, $body);
  $body = str_replace( '[email_footer]', $footer, $body);
  $body = token_replace($body, 'email_template', $template_node);
  $subject = token_replace($subject, 'email_template', $template_node);
  email_templates_send_mail( $subject, $body, $customer_email);
  drupal_set_message(t('Email confirmation to @name (@email) sent', 
    array('@name' => $customer_name, '@email' => $customer_email)));
    
  $form_state['redirect'] = 'templates/send';
}

