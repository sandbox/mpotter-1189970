This Email Templates module allows you to create different email templates that can be mailed to customers.  The customer can click a confirmation link in the message to trigger confirmation emails sent back to the technicians or customer service representatives.

This module consists of:

1) The email_template Content Type.  Create a new template using the normal Create Content/Email Template menu link.  Each email template contains the following fields:

  Template Name: The name of the template.  This is displayed in the drop-down template selection list when sending an email
  
  Subject: This is the subject of the email sent to the customer.  It can contain "tokens" (see below)
  
  Message Body:  This is the body of the email sent to the customer.  It can contain tokens.  It should contain the full HTML message to be sent to the customer.  Typically you will want to change the Input Format to Full HTML to allow this.  Use the tokens for the Header and Footer to avoid embedding tags like HTML and BODY in this field.
  
  Confirmation Recipients:  This is a list of the tech or CSR emails that the confirmation message will be sent to when the customer clicks the Confirm link in their appointment email.  A comma separated list, or place each email on a separate line.
  
2) Email Template Settings.  An Admin interface is available via admin/settings/email-templates.  The following Global settings for this module can be set:

  Header:  This is the HTML inserted by the [email_header] token.  Used to store the common email header used by all templates.  It can contain other tokens.
  
  Footer:  This is the HTML inserted by the [email_footer] token.  Used to store the common email footer used by all templates.  It can contain other tokens.
  
  Appointment Time Granularity:  Sets the number of minutes between appointment time options in the time selection drop-down field.  Defaults to 15 minutes.
  
  Customer Return Confirmation Email Subject:  This is the subject of the confirmation email sent to techs and CSRs when the customer clicks their Confirm link.  It can contain tokens.
  
  Customer Return Confirmation Email Template:  This is the body of the confirmation email sent to techs and CSRs when the customer clicks their Confirm link.  It can contain tokens.
  
  Replacement Patterns: Shows a list of all tokens defined on the system
  
3) Send Confirmation Email.  This is the main form for sending emails to customers.  It is available via the templates/send path.  The following fields are provided:
  Select email template: A drop-down list showing all template content type on the system
  Job Type: An optional field to set the [job_type] token.  
  Customer Name: The required customer name
  Customer Email: The required customer email.  Email validation is checked.
  Consultation Date: A set of 3 drop-down lists to set the date of the service appointment
  Consultation Time: A set of 3 drop-down lists to set the time of the service appointment
  Consultation End Time: A set of 3 drop-down lists to set the ending time of the service appointment
  Send Email:  This button causes the selected template to be sent to the customer email address
  
Tokens

This module defines several custom tokens that can be used in various template fields.  Tokens are accessed via the normal [tokenName] syntax.  Tokens are stored in the database and can be used anywhere.  

  [customer_name] The name of the customer
  [customer_email] The email address of the customer
  [appt_weekday] The day of week for the appointment
  [appt_month] The name of the month for the appointment
  [appt_day] The day of the month for the appointment
  [appt_time] The time of the appointment
  [appt_endtime] The ending time of the appointment
  [job_type] The Job Type entered into the Send Confirmation Email form
  [email_header] The global email header template
  [email_footer] The global email footer template
  [email_confirm_url] The URL used to for the customer Confirm link.  In the form http://hostname/templates/customer_confirm/%id
  [email_confirm_link] A proper HTML A link tag to be used as the customer Confirm link
  
Sample Email Template Body:

[email_header]
<h2>Appointment Confirmation</h2>
<p>Please review the appointment information below and click Confirm if the scheduling information is correct.</p>
Job Type: <b>[job_type]</b></br>
Customer Name: <b>[customer_name]</b></br>
Customer Email: <b>[customer_email]</b></br>
Appointment: <b>[appt_weekday] [appt_month] [appt_day] at [appt_time]</b></br>
<br/>
Confirm this appointment by clicking this link:</br>
[email_confirm_link]
[email_footer]
