<?php
// $Id: email_templates.admin.inc,v 1.0 2011/06/16 13:05:00 mpotter Exp $

/**
 * @file
 * Contains administrative page for configuring email templates.
 */

/**
 * Email Template admin form.  Set globals like header and footer  
 */
function email_templates_admin(&$form_state, $name = NULL) {
  $form['email_header'] = array(
    '#type' => 'textarea',
    '#title' => t('Header'),
    '#default_value' => variable_get('email_template_header', ''),
    '#description' => t('The HTML header used in email templates.'),
  );
  $form['email_footer'] = array(
    '#type' => 'textarea',
    '#title' => t('Footer'),
    '#default_value' => variable_get('email_template_footer', ''),
    '#description' => t('The HTML footer used in email templates.'),
  );
  $form['appt_granularity'] = array(
    '#type' => 'textfield',
    '#title' => t('Appointment Time Granularity'),
    '#default_value' => variable_get('email_template_granularity', '15'),
  );
  $form['confirm_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Customer Return Confirmation Email Subject'),
    '#default_value' => variable_get('email_template_subject', ''),
    '#description' => t('The email subject used to send confirmation emails when customer clicks the Submit button in their appointment email.'),
  );
  $form['confirm_template'] = array(
    '#type' => 'textarea',
    '#title' => t('Customer Return Confirmation Email Template'),
    '#default_value' => variable_get('email_template_confirm', ''),
    '#description' => t('The template used to send confirmation emails when customer clicks the Submit button in their appointment email.'),
  );
  $form['confirm_display'] = array(
    '#type' => 'textarea',
    '#title' => t('Customer Return Confirmation Web site message'),
    '#default_value' => variable_get('email_template_display', ''),
    '#description' => t('The template used to display the confirmation message on the web site when customer clicks the Submit button in their appointment email.'),
  );

  $form['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Make sure that the tokens you choose are available to your template when in use.'),
  );
  $form['token_help']['help'] = array(
    '#value' => theme('token_help', 'all'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function email_templates_admin_submit($form, &$form_state) {
    variable_set('email_template_header', $form_state['values']['email_header']);
    variable_set('email_template_footer', $form_state['values']['email_footer']);
    variable_set('email_template_granularity', $form_state['values']['appt_granularity']);
    variable_set('email_template_subject', $form_state['values']['confirm_subject']);
    variable_set('email_template_confirm', $form_state['values']['confirm_template']);
    variable_set('email_template_display', $form_state['values']['confirm_display']);
}